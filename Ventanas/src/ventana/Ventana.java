package ventana;


import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ventana extends JFrame {
	private JPanel panel;
	private JLabel etiquetaimagen;
	private JLabel etiqueta;
	private JButton boton;
	private int i = 0;

	public Ventana() {
		crearPanel();
		añadirImagen();
		añadirEtiqueta();
		añadirBoton();
		AñadirFondo();
		this.setVisible(true);
	}

	private void añadirBoton() {
		boton = new JButton("click");
		boton.setBounds(200, 300,100, 40);
		
		ActionListener oyente = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				i++;
				etiqueta.setText("Has apretado el boton "+ i + " veces" );
				
			}
			
			
		};
		
		boton.addActionListener(oyente);
		panel.add(boton);
		
	}
	private void crearPanel() {

		this.setSize(500, 500);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("VENTANA DE PRUEBA");
		// this.setLocation(1300,300);
		this.setLocationRelativeTo(null);
		panel = new JPanel();
		// panel.setBackground(Color.BLUE);////Azul
		panel.setLayout(null);
		this.getContentPane().add(panel);
	}

	private void añadirImagen() {

		ImageIcon imagen = new ImageIcon("images.jpg");
		ImageIcon imagenRedimensionada = new ImageIcon(
				imagen.getImage().getScaledInstance(200, 200, Image.SCALE_SMOOTH));
		etiquetaimagen = new JLabel();
		etiquetaimagen.setSize(100, 100);
		etiquetaimagen.setHorizontalAlignment(SwingConstants.CENTER);
		etiquetaimagen.setLocation(this.getWidth() / 2 - etiquetaimagen.getWidth() / 2, this.getHeight() / 12);
		etiquetaimagen.setIcon(imagenRedimensionada);
		panel.add(etiquetaimagen);
	}

	private void añadirEtiqueta() {
		etiqueta = new JLabel();
		etiqueta.setText("Winter Is Coming");
		etiqueta.setSize(340, 80);
		etiqueta.setBackground(Color.WHITE);
		etiqueta.setForeground(Color.RED);
		etiqueta.setOpaque(true);
		etiqueta.setHorizontalAlignment(SwingConstants.CENTER);
		Font f = new Font("arial", Font.BOLD, 15);
		etiqueta.setFont(f);
		Point p = etiquetaimagen.getLocation();
		etiqueta.setLocation(this.getWidth() / 2 - etiqueta.getWidth() / 2, p.y + 3 * this.getHeight() / 5);
		panel.add(etiqueta);

	}
// añadir fondo
	private void AñadirFondo() {
		ImageIcon fondo = new ImageIcon("fondo.jpg");
		JLabel etiquetafondo = new JLabel(fondo);
		etiquetafondo.setSize(this.getSize());
		//etiquetafondo.setLocation(0, 0);
		panel.add(etiquetafondo);
	}
}